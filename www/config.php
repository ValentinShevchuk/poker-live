<?php
header("Content-Type: text/html; charset=UTF-8");
SESSION_START();

$path=str_replace("/", "\\", $_SERVER['DOCUMENT_ROOT']);
define('PATH_SERVER',$path);

$_SESSION['SMARTY_DIR']='..\include_sys\smarty\\';
$_SESSION['GAME_DIR']=PATH_SERVER.'\\';
require_once('include_sys/smarty/Smarty.class.php');
$GLOBALS['smarty'] = new Smarty();
$GLOBALS['smarty']->template_dir = $_SERVER['DOCUMENT_ROOT'].'/s_templates/';
$GLOBALS['smarty']->compile_dir = $_SERVER['DOCUMENT_ROOT'].'/s_templates_c/';
?>